using System;
using System.IO;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEditor;
using MUXR.Util;

namespace MUXR.Editor
{

    public class ExportWizard : EditorWindow
    {
        readonly int maxVertices = 33000;
        private int countVertices;
        private int countTriangles;

        #region Visual Tool 
        [MenuItem("Kreis/Export Wizard")]
        static void CreateWizard()
        {
            ExportWizard window = (ExportWizard)GetWindow(typeof(ExportWizard));
            window.titleContent = new GUIContent("Export Wizard");
            window.minSize = new Vector2(200, 100);
            window.maxSize = new Vector2(200, 100);
            window.ShowUtility();
        }

        private void OnGUI()
        {
            GetCount();

            GUILayout.Space(15);

            GUILayout.BeginHorizontal("box");
            GUILayout.Label($"Current Vertices:  {countVertices}");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box");
            GUILayout.Label($"Current Triangles:  {countTriangles}");
            GUILayout.EndHorizontal();
            
            GUILayout.Space(10);

            GUILayout.BeginVertical();
            if (GUILayout.Button("Export"))
            {
                Export();
            }
            GUILayout.EndVertical();
        }

        /// <summary>
        /// Get the total Vertices and total Triangles from the scenery
        /// </summary>
        private void GetCount()
        {
            Repaint();
            MeshCountVertices meshCount = GetCounter();
            meshCount.CountVertices();
            countVertices = meshCount.totalVertices;
            countTriangles = meshCount.totalTriangles;
        }
        #endregion

        [ExecuteInEditMode]
        MeshCountVertices GetCounter()
        {
            return FindObjectOfType<MeshCountVertices>();
        }

        /// <summary>
        /// Checks integrity and Calls exporting thing
        /// </summary>
        private void Export()
        {
            GetCount();
            if (countVertices > maxVertices)
            {
                EditorUtility.DisplayDialog("Error","Too Mane Vertices, Max number: 33k","Ok");
                Debug.LogError("Too Many Vertices");
            }
            else
            {
                string path = EditorUtility.SaveFilePanel(
                    "Save package",
                    "",
                    $"KreisPackage_{Environment.UserName}",
                    "unitypackage"
                    );
                Packaging(path);
            }

        }

        /// <summary>
        /// "Compile" and Export the Package
        /// </summary>
        /// <param name="path"></param>
        private void Packaging(string path)
        {
            ExportPackageOptions exportPackageOptions = ExportPackageOptions.IncludeDependencies;
            try
            {
                AssetDatabase.ExportPackage("Assets/BaseScenery/Scenes/Staging.unity", path, exportPackageOptions);
                EditorUtility.DisplayDialog("Success","Exported Succesfully","Ok");
                Debug.Log($"Exported Successfully at {path}");
                EditorUtility.RevealInFinder(path);
                ShowPopup();
            }
            catch (Exception e)
            {
                EditorUtility.DisplayDialog("Error", "Task Failed, see console", "Ok");
                Debug.LogException(e);
            }
        }
    }
}
