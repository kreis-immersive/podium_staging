using UnityEngine;

namespace MUXR.Util
{
    public class MeshCountVertices : MonoBehaviour
    {

        [EditorAttributes.ReadOnly]  public int totalVertices = 0;
        [EditorAttributes.ReadOnly]  public int totalTriangles = 0;
        
        [ContextMenu("Count")]
        public void CountVertices()
            {
                totalVertices = 0;
                totalTriangles = 0;
               foreach(MeshFilter mf in FindObjectsOfType<MeshFilter>())
                {
                    totalVertices += mf.sharedMesh.vertexCount;
                    totalTriangles += mf.sharedMesh.triangles.Length;
                }
        }
        
    }

}
